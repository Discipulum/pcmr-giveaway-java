/** This giveaway, so far, is uncheatable.  Cheating the system is 
 * to mean that you are able to skip the questions and get the game
 * codes.
 *
 * This code must be compiled with a Java 8 compiler
 * http://www.tutorialspoint.com/codingground.htm Might be of some use
 *
 * Original idea insipired by /u/Quantum640
 **/

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

public class GiveawayBPE{

    private static GiveawayBPE instance;

    //formatter:off
    private List<String> ciphers = Arrays.asList(new String[] { 
                "DkKpZfmNynRh+XvZ3EWyQjQ64zUx12DH4S7+W22JgMKONagGy5a6tWgHVAA=", 
                "TUPYNipQqS3dbuGBTsdscd4s1pTe6dcy4cjBB4OBPRFuHCuNRxXey/Ca0tx71YL9SKL8+INJVJz1Zg9+2QJ8IdbKDVv0mLcZZyV/pNRKXpQPaK03LpWhog==",
                "k+jMer9QxxppaPBK7s+DM8HY4x4IuI1KgGnnkH/Nu/vhSXqAZBAuUPzgy14=",
                "O06mYBhqBlxpfDh1+MjK+CSTrknJTSgzKldOK8LokBgeBPzCb4fpv2LaEciZwaoI4eyolyQ6+aB3PDrN6+eKWnWalR4idWE837vJXu9FJBE=",
                "0OilTjcVr4eb/siJpOK9nT08Xjj2Mubbmccm3LRC+IFjafR2zo8K8wRPQJoTxwhnNZB+DSt/L/cFkjv7rGFjz2Wff5opLGNKM7uQcsnNkDWibS1sj4UvGg==",
                "67KW9TWT4Eczb1KtBjPumls4D5Csach/dLRuKoQLCmuuYWTlYcBcXn4IGwRQslyN4IohEkUuogU2lucyu2wuiGDEWQ2VaMwzN5eJzAbZwQE=",
                "rxImMl5xrP7kkP23CN1+FHvUYkI1CtwQtQ94OBLUkKf+d+GkctTTHiS+0nrih4eauTvZujCVEVg="
                });
    //formatter:on
    
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); //Basic Input

    String currentQuestion = new String(Base64.getDecoder().decode("V2hhdCBzdWJyZWRkaXQgaXMgdGhpcyBnaXZlYXdheSBmcm9tKF9fX19fX19fX19fKT8=")); 
            //Starting Question: What subreddit is this giveaway from(___________)? Answer provides key to decode second key.
    String nextQuestionDecoded = new String();
            //Stores the next question
    String answer = new String();
            //String that answer will be stored in.
     
            //Displays the first question

    public static void main(String[] args) {
        instance = new GiveawayBPE();
        
        try {
            instance.init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() throws Exception {
        System.out.println(currentQuestion + "\n");
        for (String cipher : ciphers) { 
            //Loop for as many times as there are positions in String[] ciphers
            questionasker(cipher);
            //Once there are not more questions, you should have the key to decode the Game Code,
        }
        System.out.println("\n\n Press enter to end the program.");
        answer = br.readLine().toLowerCase();
    }
    
    public void questionasker(String cipher) throws Exception {
        while (true) {
                System.out.print("Type the answer in relation to the text above: ");
                answer = br.readLine().toLowerCase(); //This is getting the answer to the questions.
                try { //attempting to decode the next question
                    byte[] key = answer.getBytes(); //Convert answer to a Byte Array to be decoded(if correct key is given)
                    nextQuestionDecoded = new String(decrypt(Base64.getDecoder().decode(cipher), key)); //Decodes the next questions
                } catch (RuntimeException e) {
                    System.out.println(" \n Incorrect/Null Answer! Please Try Again! \n "); 
                        //If you get the wrong answer, this is what you will get.
                        //Provide the correct answer to move on.
                        questionasker(cipher);
                        break;
                }
                currentQuestion = nextQuestionDecoded; 
                    //If no Exception is caught, assign decoded question to the current question, then loop
                System.out.println(currentQuestion + "\n"); //Displays the current question
                break;
            }
    }
    /**-*-*-*-*-*-*-*-*-*-*-*-*-*-*--*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
     *
     * XXTEA encryption arithmetic library.
     *
     * Copyright: Ma Bingyao <andot@ujn.edu.cn>
     * Version: 3.0.2
     * LastModified: Apr 12, 2010
     * This library is free.  You can redistribute it and/or modify it under GPL.
     *-*-*-*-*-*-*-*-*-*-*-*-*-*-*--*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**/

    private static final int delta = 0x9E3779B9;

    private static final int MX(int sum, int y, int z, int p, int e, int[] k) 
    {
        return (z >>> 5 ^ y << 2) + (y >>> 3 ^ z << 4) ^ (sum ^ y) + (k[p & 3 ^ e] ^ z);
    }

    /**
     * Encrypt data with key.
     *
     * @param data
     * @param key
     * @return
     */
    public static final byte[] encrypt(byte[] data, byte[] key) 
    {
        if (data.length == 0) {
            return data;
        }
        return toByteArray(
            encrypt(toIntArray(data, true), toIntArray(key, false)), false);
    }

    /**
     * Decrypt data with key.
     *
     * @param data
     * @param key
     * @return
     */
    public static final byte[] decrypt(byte[] data, byte[] key) 
    {
        if (data.length == 0) {
            return data;
        }
        return toByteArray(
            decrypt(toIntArray(data, false), toIntArray(key, false)), true);
    }

    /**
     * Encrypt data with key.
     *
     * @param v
     * @param k
     * @return
     */
    private static final int[] encrypt(int[] v, int[] k) 
    {
        int n = v.length - 1;

        if (n < 1) {
            return v;
        }
        if (k.length < 4) {
            int[] key = new int[4];

            System.arraycopy(k, 0, key, 0, k.length);
            k = key;
        }
        int z = v[n], y = v[0], sum = 0, e;
        int p, q = 6 + 52 / (n + 1);

        while (q-- > 0) {
            sum = sum + delta;
            e = sum >>> 2 & 3;
            for (p = 0; p < n; p++) {
                y = v[p + 1];
                z = v[p] += MX(sum, y, z, p, e, k);
            }
            y = v[0];
            z = v[n] += MX(sum, y, z, p, e, k);
        }
        return v;
    }

    /**
     * Decrypt data with key.
     *
     * @param v
     * @param k
     * @return
     */
    private static final int[] decrypt(int[] v, int[] k) 
    {
        int n = v.length - 1;

        if (n < 1) {
            return v;
        }
        if (k.length < 4) {
            int[] key = new int[4];

            System.arraycopy(k, 0, key, 0, k.length);
            k = key;
        }
        int z = v[n], y = v[0], sum, e;
        int p, q = 6 + 52 / (n + 1);

        sum = q * delta;
        while (sum != 0) {
            e = sum >>> 2 & 3;
            for (p = n; p > 0; p--) {
                z = v[p - 1];
                y = v[p] -= MX(sum, y, z, p, e, k);
            }
            z = v[n];
            y = v[0] -= MX(sum, y, z, p, e, k);
            sum = sum - delta;
        }
        return v;
    }

    /**
     * Convert byte array to int array.
     *
     * @param data
     * @param includeLength
     * @return
     */
    private static final int[] toIntArray(byte[] data, boolean includeLength) 
    {
        int n = (((data.length & 3) == 0)
                ? (data.length >>> 2)
                : ((data.length >>> 2) + 1));
        int[] result;

        if (includeLength) {
            result = new int[n + 1];
            result[n] = data.length;
        }
        else {
            result = new int[n];
        }
        n = data.length;
        for (int i = 0; i < n; i++) {
            result[i >>> 2] |= (0x000000ff & data[i]) << ((i & 3) << 3);
        }
        return result;
    }

    /**
     * Convert int array to byte array.
     *
     * @param data
     * @param includeLength
     * @return
     */
    private static final byte[] toByteArray(int[] data, boolean includeLength) 
    {
        int n = data.length << 2;

        if (includeLength) {
            int m = data[data.length - 1];

            if (m > n) {
                return null;
            }
            else {
                n = m;
            }
        }
        byte[] result = new byte[n];

        for (int i = 0; i < n; i++) {
            result[i] = (byte) ((data[i >>> 2] >>> ((i & 3) << 3)) & 0xff);
        }
        return result;
    }
}