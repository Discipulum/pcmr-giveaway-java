/** This giveaway, so far, is uncheatable.  Cheating the system is 
 * to mean that you are able to skip the questions and get the game
 * codes.
 *
 * This code must be compiled with a Java 8 compiler
 * http://www.tutorialspoint.com/codingground.htm Might be of some use
 *
 * Original idea insipired by /u/Quantum640
 **/

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

public class GiveawayHBWSE{

    private static GiveawayHBWSE instance;

    //formatter:off
    private List<String> ciphers = Arrays.asList(new String[] { 
                "/X8pfWDAW+RJF1C7kUt6JmH0ZBm/Nn7drYwVXWWAGkXOjNVGdG+XR4MA8OxyUSpaBW5mCQ==",
                "9a0bFnFrNTc2MiiBcMfF0jJMeD5Olob9akZix39kvd0tRxj+i7HRd55KjsFzUbu473Zp/yClHZYqA9WyZlY8oU0IQMYWgV6bEgt1Pi32UI8=",
                "VVkPToHGA0fiRASCihI0EgsUKPsKIX4cXbtzgxADJO7p7+BIJPpQqwYIx1Fo3o2DRUmRUTiqbkDDyp7arvg/zIbF73wK/mAY56LGqQkT/mZdOgpn0HQYTnJMqnAgVEqy/AgJdMBV7sM=",
                "OebaAOF0yV5Fz2jCvqLleRoWZUOo8MB/r17KzAfRfLYoLXu73dme/SC65JNQbUnMsfOb3V+sO87pKGtGiGfrzg==",
                "te0x60UCqRgK8rEjPs7Oo97X1yVl9F2Wqk9ZLuKS88Y9260GqVTHxOahY1pxQYQ6daM8wPbSULunPUZlTg7CB4CSD7V+ofA01FrnOGsgMmE=",
                "MRsIuuNxiPCTD44xIwfxOXXXGerKUnk43guD3TxbbWjd0C5WyTVBQgm1fVScPU8QaGNk7xIDlICUYYI3XAIqDjcip2I=",
                "//vr921AKooYGAIVTxncdql9g6l0+Ab4GSSc0IFtalpzqnuWTgE6g1u9ZvqHJXXAKTvwpA3vth+nk6dBi0lITw==",
                "NT/J0gWHFxzENRq3ZuwhMHvO36lPa0H96lEvF3wjbG/v8WHS5sGMjmkGDRNgHLDKWJtM/YFmtPtULCxfCMgoU11/Iic=",
                "Jnz9gykDoK8yw+9sR2LXaPbmkqqprWEAusobJQCpCYO3Vurc",
                "1ru1qtIrclO+xJLyDgRmsA/QW6S+K8DjufiXgB8hgK0BOZ7EfE2nRTjqfZkhQH6hAQSMJ/lw6VDGHrUFDWf7OqNQNR0uMjE4pVOErdjmeDVWWPRT+q2I809HP7iVE4+MtMtwTA==",
                "yGh5HCyIDvPGHEPATCG44DXktn5VLClAfOyMswRQTwLzPayfUMSbklcb59SDYHgGWMBIGEodOglWBGvziBnwiA==",
                "rAEy0ggTVctIR3qQKZxQPCPA3crf4nudhM4clIxskjwDll6Ysz79OE/zuaaIxZuaQSmY6ZN6hfI=",
                "7VXjyVyuOrla8njctn1F5LqCUDtTOcRpv2YRpzyhDC0IvcMhYZpWdwKSYe2gVXiMvq/8zBFAJE8=",
                "nhgCsuMr3Ex7/4+F6LUA8V6RGRCg6FfiyH31jt4HDEf4zMOqY+rVnQQrPd8egseZjesz9tou1AXPat56dxemag==",
                "scR0O77B294pxAq1LFmTORnCoFSFLzobI0QcwMn997gpnKIPWihVSB5jObU4vColu9MzR/+QCJs=",
                "kWuJ7B6ZCYqln0rDgwnXJ+vSzSsVtqEL1JMMCadjpX7+//LTwabViZmDu+jknG+dMsqqxWJFy8Lr2PpFzVpUrB7u/7souomS6/0iUibwTz4s58MlhWBkFw==",
                "S4atMzQERCE8xLCMIGmBg+8Ki2ogpR7xFvJvzGshgy4aPgrb4GRf1dnFXIywfLI+XFvEVHx6tmwSRDF0Vu4ZlYpPbvoZRcN1+CxDXijnCKcMZoq0",
                "eWmSwOBlj7fJz/wKilG4DmQJeJP70NQdKTzZjw+WyceegAVv63ly22YtLy1Ddj32MhLqxrQ0HP+Ye6ddWRv6Qm9nYTnayCOjZhwJ0L5hcMTton/u0qEEbJHpARc6Y2NmwZfP9GyBkqDgX9cpWZ5jDL8FYOBm+k5WGzBnKvhkkMZnUZpvQv5pwV7TRnLxs8w3vS1HS8TEIFp1U4pX+M6Tbzr0DcEqt3XA",
            });
    //formatter:on

    BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); //Basic Input

    String currentQuestion = new String(Base64.getDecoder().decode("V2hhdCBzdWJyZWRkaXQgaXMgdGhpcyBnaXZlYXdheSBmcm9tKF9fX19fX19fX19fKT8=")); 
    //Starting Question: What subreddit is this giveaway from(___________)? Answer provides key to decode second key.
    String nextQuestionDecoded = new String();
    //Stores the next question
    String answer = new String();
    //String that answer will be stored in.

    //Displays the first question

    public static void main(String[] args) {
        instance = new GiveawayHBWSE();

        try {
            instance.init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() throws Exception {
        System.out.println(currentQuestion + "\n"); //Prints the Where is giveaway from question.
        for (String cipher : ciphers) { 
            //Loop for as many times as there are positions in String[] ciphers
            questionasker(cipher); //Sends to a seperate method, which allows a question to be reanswer without being asked.
            //Once there are not more questions, you should have the key to decode the Game Code,
        }
        System.out.println("\n\n Press enter to end the program."); //Stop the Program from auto exiting when running in .BAT
        answer = br.readLine().toLowerCase(); //Basically a pause until ENTER is hit line.
    }

    public void questionasker(String cipher) throws Exception {
        while (true) {
            System.out.print("Type the answer in relation to the text above: ");
            answer = br.readLine().toLowerCase(); //This is getting the answer to the questions.
            try { //attempting to decode the next question
                byte[] key = answer.getBytes(); //Convert answer to a Byte Array to be decoded(if correct key is given)
                nextQuestionDecoded = new String(decrypt(Base64.getDecoder().decode(cipher), key)); //Decodes the next questions
            } catch (RuntimeException e) {
                System.out.println(" \n Incorrect/Null Answer! Please Try Again! \n "); 
                //If you get the wrong answer, this is what you will get.
                //Provide the correct answer to move on.
                questionasker(cipher);
                break;
            }
            currentQuestion = nextQuestionDecoded; 
            //If no Exception is caught, assign decoded question to the current question, then loop
            System.out.println(currentQuestion + "\n"); //Displays the current question
            break;
        }
    }
    /**-*-*-*-*-*-*-*-*-*-*-*-*-*-*--*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
     *
     * XXTEA encryption arithmetic library.
     *
     * Copyright: Ma Bingyao <andot@ujn.edu.cn>
     * Version: 3.0.2
     * LastModified: Apr 12, 2010
     * This library is free.  You can redistribute it and/or modify it under GPL.
     *-*-*-*-*-*-*-*-*-*-*-*-*-*-*--*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**/

    private static final int delta = 0x9E3779B9;

    private static final int MX(int sum, int y, int z, int p, int e, int[] k) 
    {
        return (z >>> 5 ^ y << 2) + (y >>> 3 ^ z << 4) ^ (sum ^ y) + (k[p & 3 ^ e] ^ z);
    }

    /**
     * Encrypt data with key.
     *
     * @param data
     * @param key
     * @return
     */
    public static final byte[] encrypt(byte[] data, byte[] key) 
    {
        if (data.length == 0) {
            return data;
        }
        return toByteArray(
            encrypt(toIntArray(data, true), toIntArray(key, false)), false);
    }

    /**
     * Decrypt data with key.
     *
     * @param data
     * @param key
     * @return
     */
    public static final byte[] decrypt(byte[] data, byte[] key) 
    {
        if (data.length == 0) {
            return data;
        }
        return toByteArray(
            decrypt(toIntArray(data, false), toIntArray(key, false)), true);
    }

    /**
     * Encrypt data with key.
     *
     * @param v
     * @param k
     * @return
     */
    private static final int[] encrypt(int[] v, int[] k) 
    {
        int n = v.length - 1;

        if (n < 1) {
            return v;
        }
        if (k.length < 4) {
            int[] key = new int[4];

            System.arraycopy(k, 0, key, 0, k.length);
            k = key;
        }
        int z = v[n], y = v[0], sum = 0, e;
        int p, q = 6 + 52 / (n + 1);

        while (q-- > 0) {
            sum = sum + delta;
            e = sum >>> 2 & 3;
            for (p = 0; p < n; p++) {
                y = v[p + 1];
                z = v[p] += MX(sum, y, z, p, e, k);
            }
            y = v[0];
            z = v[n] += MX(sum, y, z, p, e, k);
        }
        return v;
    }

    /**
     * Decrypt data with key.
     *
     * @param v
     * @param k
     * @return
     */
    private static final int[] decrypt(int[] v, int[] k) 
    {
        int n = v.length - 1;

        if (n < 1) {
            return v;
        }
        if (k.length < 4) {
            int[] key = new int[4];

            System.arraycopy(k, 0, key, 0, k.length);
            k = key;
        }
        int z = v[n], y = v[0], sum, e;
        int p, q = 6 + 52 / (n + 1);

        sum = q * delta;
        while (sum != 0) {
            e = sum >>> 2 & 3;
            for (p = n; p > 0; p--) {
                z = v[p - 1];
                y = v[p] -= MX(sum, y, z, p, e, k);
            }
            z = v[n];
            y = v[0] -= MX(sum, y, z, p, e, k);
            sum = sum - delta;
        }
        return v;
    }

    /**
     * Convert byte array to int array.
     *
     * @param data
     * @param includeLength
     * @return
     */
    private static final int[] toIntArray(byte[] data, boolean includeLength) 
    {
        int n = (((data.length & 3) == 0)
                ? (data.length >>> 2)
                : ((data.length >>> 2) + 1));
        int[] result;

        if (includeLength) {
            result = new int[n + 1];
            result[n] = data.length;
        }
        else {
            result = new int[n];
        }
        n = data.length;
        for (int i = 0; i < n; i++) {
            result[i >>> 2] |= (0x000000ff & data[i]) << ((i & 3) << 3);
        }
        return result;
    }

    /**
     * Convert int array to byte array.
     *
     * @param data
     * @param includeLength
     * @return
     */
    private static final byte[] toByteArray(int[] data, boolean includeLength) 
    {
        int n = data.length << 2;

        if (includeLength) {
            int m = data[data.length - 1];

            if (m > n) {
                return null;
            }
            else {
                n = m;
            }
        }
        byte[] result = new byte[n];

        for (int i = 0; i < n; i++) {
            result[i] = (byte) ((data[i >>> 2] >>> ((i & 3) << 3)) & 0xff);
        }
        return result;
    }
}